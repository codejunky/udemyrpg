﻿using System.Runtime.CompilerServices;
using UnityEngine;

public class CameraRaycaster : MonoBehaviour {

    [SerializeField] float distanceToBackground = 100f;
    Camera viewCamera;

    RaycastHit hit;

    public RaycastHit currentHit {
        get { return hit; }
    }

    Layer layerHit;

    public Layer currentLayerHit {
        get { return layerHit; }
    }

    public delegate void OnLayerChange(Layer currentLayer); //declare new delegate type
    public event OnLayerChange layerChangeObservers; // instantiate an observer set


    void Start() {
        viewCamera = Camera.main;
    }

    void LateUpdate() {
        var prevLayer = layerHit;
        var hit = RaycastAllLayers();
        if (hit.HasValue) {
            this.hit = hit.Value;
            layerHit = (Layer) hit.Value.transform.gameObject.layer;
        }
        else {
            // Otherwise return background hit
            this.hit.distance = distanceToBackground;
            layerHit = Layer.RaycastEndStop;
        }

        if (prevLayer != layerHit) {
            layerChangeObservers(layerHit); //call the delegates
        }
    }


    RaycastHit? RaycastAllLayers() {
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit; // used as an out parameter

        bool hasHit = Physics.Raycast(ray, out hit, distanceToBackground);
        if (hasHit) {
            return hit;
        }

        return null;
    }
}