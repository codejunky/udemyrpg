﻿internal static class Tags {
	public const string PLAYER="Player";
	public const string RESPAWN="Respawn";
	public const string FINISH="Finish";
	public const string EDITORONLY="EditorOnly";
	public const string MAINCAMERA="MainCamera";
	public const string GAMECONTROLLER="GameController";
}