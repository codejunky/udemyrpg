using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(ThirdPersonCharacter))]
public class PlayerMovement : MonoBehaviour {
    [FormerlySerializedAs("walkMoveStopRadius")] [SerializeField] float movementDeadzone = 0.1f;
    [SerializeField] float attackMoveStopRadius = 5f;

    
    ThirdPersonCharacter character; // A reference to the ThirdPersonCharacter on the object
    CameraRaycaster cameraRaycaster;
    Vector3 currentClickTarget, clickPoint;

    bool isDirectMode = false;

    private void Start() {
        cameraRaycaster = Camera.main.GetComponentInParent<CameraRaycaster>();
        character = GetComponent<ThirdPersonCharacter>();
        currentClickTarget = transform.position;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.G)) {
            // G for gamepad TODO: allow player to map later
            isDirectMode = !isDirectMode; // toggle mode
            currentClickTarget = transform.position; // reset click target
        }

        ProcessMouseInput();
    }


    void FixedUpdate() {
        if (isDirectMode) {
            ProcessDirectMovement();
        }
        else {
            ProcessMouseMovement();
        }
    }

    void ProcessDirectMovement() {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        
        print(h + " " + v);
        
        var camForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        var move = v*camForward + h*Camera.main.transform.right;
        
        character.Move(move, false, false);
    }

    void ProcessMouseMovement() {
        var playerTargetDistance = 
            new Vector3(currentClickTarget.x, 0, currentClickTarget.z) 
            - new Vector3(transform.position.x, 0, transform.position.z); //TODO: creating a new vector every frame might not be good for performance
        
        if (playerTargetDistance.magnitude >= movementDeadzone) {
            character.Move(playerTargetDistance, false, false);
        }
        else {
            character.Move(Vector3.zero, false, false);
        }
    }


    void ProcessMouseInput() {
        if (Input.GetMouseButtonDown(0)) {
            clickPoint = cameraRaycaster.currentHit.point;
            print("Cursor raycast hit " + cameraRaycaster.currentLayerHit);
            switch (cameraRaycaster.currentLayerHit) {
                case Layer.Walkable:
                    currentClickTarget = clickPoint;
                    print("Moving to target");
                    break;
                case Layer.Enemy:
                    currentClickTarget = ShortDestination(clickPoint, attackMoveStopRadius);
                    print("Moving to enemy");
                    break;
                case Layer.Default:
                    print("Default Layer Clicked");
                    break;
                case Layer.RaycastEndStop:
                    print("No layer was hit (probably off-grid)");
                    break;
                default:
                    print("Unknown layer clicked");
                    break;
            }
        }
    }

    Vector3 ShortDestination(Vector3 destination, float shortening) {
        var reductionVector = (destination - transform.position).normalized * shortening;
        return destination - reductionVector;
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.black;
        Gizmos.DrawLine(transform.position, clickPoint);
        Gizmos.DrawSphere(clickPoint, 0.1f);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(currentClickTarget, 0.05f);
    }
}