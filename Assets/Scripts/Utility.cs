﻿public enum Layer
{
    Default = 0,
    TransparentFX = 1,
    IgnoreRaycast = 2,
    Water = 4,
    UI = 5,
    PostProcessing = 8,
    Walkable = 9,
    Enemy = 10,
    
    RaycastEndStop = -1
}
