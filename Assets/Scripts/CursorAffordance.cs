﻿using UnityEngine;

[RequireComponent(typeof(CameraRaycaster))]
public class CursorAffordance : MonoBehaviour {
    [SerializeField] Texture2D walkCursor;
    [SerializeField] Texture2D enemyCursor;
    [SerializeField] Texture2D defaultCursor;
    [SerializeField] Texture2D unknownCursor;

    CameraRaycaster cameraRaycaster;
    Vector2 cursorHotspot = Vector2.zero;


    // Use this for initialization
    void Start() {
        cameraRaycaster = GetComponent<CameraRaycaster>();
        cameraRaycaster.layerChangeObservers += OnLayerChange;
    }

    void OnLayerChange(Layer currentLayer){
        switch (currentLayer) {
            case Layer.Walkable:
                Cursor.SetCursor(walkCursor, cursorHotspot, CursorMode.Auto);
                break;
            case Layer.Enemy:
                Cursor.SetCursor(enemyCursor, cursorHotspot, CursorMode.Auto);
                break;
            case Layer.Default:
                Cursor.SetCursor(defaultCursor, cursorHotspot, CursorMode.Auto);
                break;
            case Layer.RaycastEndStop:
                Cursor.SetCursor(unknownCursor, cursorHotspot, CursorMode.Auto);
                break;
            default:
                Debug.LogError("Unknown Cursor Layer!!!");
                break;
        }
        
    }
    
    //TODO consider de-registering OnLayerChange on leaving all game scenes

}