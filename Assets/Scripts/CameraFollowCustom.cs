﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowCustom : MonoBehaviour
{
    private GameObject player;

    // Use this for initialization
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag(Tags.PLAYER);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void LateUpdate()
    {
        transform.position = player.transform.position;
    }
}